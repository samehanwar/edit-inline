(function($){
    
    $.fn.defaults = {
        type : 'text',
        selectOptions : "",
        itemsExtraData : [],
        extrafields : '',
        extrafieldsTemplate : "",
        extrafieldsOptions: {},
        callback : function(){},
        url : '',
        postData : {},
        templateContainer : 'input-form-wrapper'
    };
    
    $.fn.editinline = function(options){
        var setting = $.extend({},$.fn.defaults, options);
        var attr = $(this).attr('id');
        if (typeof attr !== typeof undefined && attr !== false) {
           var targetElement = "#" + this.attr('id');
        }else{
            var target = $(this).data('inline-edit');
            targetElement = '.'+target;
        }
        
        jQuery(document).on({
            mouseenter: function () {
                var elePos = $(this).position(),
                    eleWidth = $(this).outerWidth();
                    if( $(this).width() == 0) { eleWidth = '200'};
                $(this).toggleClass('inline-edit-visible').on('click',function(e){
                    e.preventDefault();
                    e.stopImmediatePropagation();
                    $('.input-form-wrapper').prev('.inline-edit').css("visibility","visible").end().remove();
                    var fieldValue = $(this).text();
                    var template  = '';
                        template += '<div class="input-form-wrapper" id="input-form-wrapper" style="position:absolute;left:'+ elePos.left +'px;top:'+ elePos.top+'px;width:'+ eleWidth +'px">';
                        template += renderTemplate(setting.type,fieldValue,targetElement,setting.selectOptions);
                        template += '<div class="check-buttons"><button class="btn btn-info" id="approve-button" value="yes"><i class="fa fa-check"></i></button><button class="btn btn-info" id="reject-button" value="no"><i class="fa fa-remove"></i></button></div></div>';
                        
                    $(this).css("visibility","hidden").after(template);
                    
                    var formContainer = $("." + setting.templateContainer);
                    formContainer.find('select#basic').selectpicker('refresh');
                    formContainer.find('.dropdown-toggle').dropdown();
                    
                    formContainer.find('.datepicker').datepicker({
                        format: 'yyyy-mm-dd'
                    });
                    
                    $('.typeahead').bind('typeahead:select', function(ev, suggestion) {
                        window.typeaheadSelectValue = suggestion;
                    });

                    initializeUserCallbackFunction();

                    formContainer.find('input').select();
                    formContainer.find('.check-buttons').children('button').on('click',function(e){
                        e.preventDefault();
                        var buttonResult = e.target.value;
                        var inputKey = "";
                        
                        if(setting.extrafields == 'auto-complete'){
                            if(typeaheadSelectValue.name){
                                var inputValue = typeaheadSelectValue.name;
                                    if(typeaheadSelectValue.shortName){
                                        inputValue +=  " ("+ typeaheadSelectValue.shortName +")";
                                    }
                            }else{
                                inputValue = typeaheadSelectValue.firstName + " " + typeaheadSelectValue.lastName;
                            }
                            inputKey = formContainer.find('#contact-company-id').val();
                        }else if(setting.extrafields == 'dropdown-select'){
                            var eleAttr = formContainer.find('.bootstrap-select select option').attr('data-icon');
                            if(typeof eleAttr !== typeof undefined && eleAttr !== false){
                                var attrIcon = '<i class="'+eleAttr+'"></i>';
                                inputValue = attrIcon + formContainer.find('.bootstrap-select span.filter-option').text();
                            }else{
                                inputValue = formContainer.find('.bootstrap-select span.filter-option').text();
                            }
                            inputKey = formContainer.find('select').val();
                            console.log(inputKey);
                            console.log(inputValue);
                        }else if(setting.type == 'datetime'){
                            var stringDate = formContainer.find('div.date input').val();
                            var parts = stringDate.split('/');
                                inputValue =  parts[2]+'-'+parts[0]+'-'+parts[1];
                        }else if(setting.type == 'select'){
                            inputValue = formContainer.find('select > option:selected').text();
                            inputKey = formContainer.find('select').val();
                        }else{
                            inputValue = formContainer.find('input,textarea,select').val();
                        }
                        
                        if(buttonResult == 'no'){
                            formContainer.remove();
                            jQuery(targetElement).css("visibility","visible");
                        }
                        else{
                            formContainer.remove();
                            jQuery(targetElement).css("visibility","visible").html(inputValue);
                            if(setting.type == 'select' || setting.extrafields == 'dropdown-select'){
                                saveData(inputKey);
                            }else{
                                saveData(inputValue);
                            }
                        }
                    });
                });
            },
            mouseleave: function() {
                $(this).removeClass('inline-edit-visible');
            }
        },targetElement);
        
        $('body').on('click',function(e)
        {
            if(!$(e.target).is('input, select, textarea, div.bootstrap-select ul li *, span.input-group-addon, .note-editor *, .note-editable, .note-toolbar-wrapper *, span.glyphicon, div.tt-suggestion, .on-the-fly-add-btn, .on-the-fly-add-btn a, .on-the-fly-add-btn i')){
                $('.input-form-wrapper').remove();
                jQuery(targetElement).css("visibility","visible");
            }
        });
        
        function initializeUserCallbackFunction()
        {
            if($.isFunction(setting.callback)){
                return setting.callback.call(this);
            }
        }
        
        function renderTemplate(type, value, targetElement, options)
        {
            var type, value, options,  template = "";
            switch(type){
                case 'text':
                    template += '<input type="text" class="form-control" name="fieldHeader" value="'+ value +'" selected />';
                    break;
                case 'textarea':        
                    template += '<textarea class="summernote" style="height:200px;width:100%;">'+ value +'</textarea>';
                    break;
                case 'select':        
                    if(typeof setting.extrafields !== 'undefined' && setting.extrafields.length > 0){
                        if(setting.extrafields == 'auto-complete'){
                            template += '<select class="form-control" id="client-type">';
                            template += getListItems(options, type = 'select');
                            template += '</select>';
                            template += '<div id="matter-opponent">';
                            template += '<input type="hidden" name="contact_company_id" id="contact-company-id" value="" />';
                            template += '<input class="typeahead form-control" type="text" id="client-lookup" placeholder="type here ">';
                            template += '<div data-field="contact_company_id" class="inline-error hide"></div>';
                            template += '</div>';
                        }else if(setting.extrafields == 'add-on-the-fly-button'){
                            if(setting.extrafieldsOptions != 'undefined'){
                                var selectTypes = setting.extrafieldsOptions.selectObject;
                            }
                            template += '<div class="col-md-10 no-padding" style="padding-right:5px;"><select class="form-control" id="'+ jQuery(targetElement).selector.substr(1) + '" data-field="administration-'+selectTypes+'">';
                            template += getListItems(options, type = 'select');
                            template += '</select></div>';
                            template += '<div class="col-md-2 no-padding"><div class="on-the-fly-add-btn"';
                            if(setting.extrafieldsTemplate != 'undefined'){
                                template += setting.extrafieldsTemplate
                            }
                            template += '</div></div>';
                        }
                    }
                    else{
                        template += '<select class="form-control" id="'+ jQuery(targetElement).selector.substr(1) + '">';
                        template += getListItems(options, type = 'select');
                        template += '</select>';
                    }
                    break;
                case 'auto-complete':
                    template += '<div class="autocomplete-lookUp">';
                    template += '<input type="hidden" name="requestedBy" id="requestedBy" value="" />';
                    template += '<input type="text" name="requestedByName" id="requestedByLookup" value="" class="lookup form-control" placeholder="start_typing" title="start_typing"  />';
                    template += '<div data-field="requestedByFrom" class="inline-error hide"></div>';
                    template += '</div>';        
                    break;
                case 'select-lookup':        
                    template += '<select id="basic" class="selectpicker form-control" data-live-search="true">';
                    template += getListItems(options, type = 'select');
                    template += '</select>';
                    break;
                case 'datetime':        
                    template += '<div class="form-group">';
                    template += '<div class="input-group  date" data-provide="datepicker">';
                    template += '<input type="text" class="form-control" />';
                    template += '<span class="input-group-addon">';
                    template += '<span class="glyphicon glyphicon-calendar"></span>';
                    template += '</span></div></div>';
                    break;
                default:
                   template;
            }
            return template;
        }
        
                                    
        function templateFactory(templateType, options,targetElement)
        {
            var templateType, type, theme = "";
            if(templateType == 'select'){
                getListItems(options, type = 'select');
            }
            return theme += '<select class="form-control" id="'+ jQuery(targetElement) + '">'; 
                   theme += getListItems(options, type = 'select');
                   theme += '</select>'; 
        }
        
        function getListItems(options, type)
        {
            var options, type, listItems = "", attributes = "";
            if(options instanceof  Array){
                if(type == 'select'){
                    for(var i=0; i < options.length; i++){
                         listItems  += '<option value="'+options[i]+'">'+ options[i] +'</option>';
                    }
                }else{
                    for(var i=0; i < options.length; i++){
                         listItems  += '<li>'+ options[i] +'</li>';
                    }
                }
            }else{
                jQuery.each(options, function(element, value){
                    if( typeof value == 'object' ){
                        var elementvalue = element;
                        jQuery.each(value, function(attr, attrValue){
                            attributes = attr + '="' + attrValue + '"' ;
                        });
                    }else{
                        elementvalue = value;
                    }
                    listItems  += '<option value="'+element+'" '+attributes+'>'+ elementvalue +'</option>';
                });
            }
            return listItems;
        }
        
        function saveData(value){
            var data = setting.postData;
            setting.postData.action = 'inlineEditRequest';
            setting.postData.fieldValue = value;
            console.log(value);
            jQuery.ajax({
                beforeSend: function () {
                    jQuery('#page-loader').show();
                },
                url: getBaseURL() + 'cases/edit',
                type: 'POST',
                data: data,
                success: function(response){
                    console.log(response);
                    switch (response.status) {
                        case true:	// saved successfuly
                            pinesMessage({ty: 'success', m: _lang.feedback_messages.updatesSavedSuccessfully});
                            break;
                        case false:
                            var errorMsg = '';
                            for (i in response.validationErrors) {
                                jQuery('#' + i, document.body).addClass('invalid').focus(function () {
                                    jQuery(this).removeClass('invalid');
                                });
                                errorMsg += '<li>' + response.validationErrors[i] + '</li>';
                            }
                            if (errorMsg != '') {
                                pinesMessage({ty: 'error', m: '<ul>' + errorMsg + '</ul>'});
                            }
                            break;
                        default:
                            break;
                    }
                },
                complete: function () {
                    jQuery('#page-loader').hide();
                }
            });
        }
    };
}(jQuery));